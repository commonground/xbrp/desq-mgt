import 'package:xbrpmgt/src/models/event.dart';
import 'package:xbrpmgt/src/models/zone.dart';
import 'package:xbrpmgt/src/xbrp/api.dart';

class EventStore {
  List<Zone> zones;
  Map<Zone, List<Event>> events = {};

  Future<bool> init() async {
    // Load all zones and their events
    this.zones = await fetchZones();
    for (Zone zone in this.zones) {
      List<Event> events = await fetchEvents(zone);
      this.events[zone] = events;
    }
    return true;
  }

  String getPersonName(String eid) {
    // Search for a born event for the given eid
    for (Zone zone in this.zones) {
      Event found = this.events[zone].firstWhere(
          (event) => event.kind == 'born' && event.data['ChildEID'] == eid,
          orElse: () => null);
      if (found != null) {
        return "${found.data['Firstname']} ${found.data['Lastname']}";
      }
    }
  }
}

// Global event store
EventStore eventStore = EventStore();
