import 'package:intl/intl.dart';

class Zone {
  final String zid;
  final String name; // e.g. Amsterdam
  final int port; // e.g. 3001

  Zone({this.zid, this.name, this.port});

  String get id => this.zid;

  String get gemeente => this.name;

  factory Zone.fromJson(Map<String, dynamic> json) {
    // Prepare code for import zones from XBRP API
    return Zone(
      zid: json['zid'],
      name: json['name'],
      port: json['port'],
    );
  }
}
