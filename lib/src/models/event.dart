import 'package:xbrpmgt/src/models/event_store.dart';

class Event {
  // Base class for events
  final String kind;
  final DateTime materialDate;
  final String zid;
  final Map<String, dynamic> data;

  Event({this.kind, this.materialDate, this.zid, this.data});

  Map<String, String> get contents {
    return _dynamicToMap(data);
  }

  String _getPersonName(String eid) {
    return eventStore.getPersonName(eid) ?? "[ ${eid.substring(0, eid.indexOf('-'))} ]";
  }

  String _getAddress() {
    var addr = this.data['Address'];
    if (addr != null) {
      return "${addr['streetname']} ${addr['housenumber']}${addr['housenumber_addition']}, ${addr['zipcode']} ${addr['city']}";
    }
    return null;
  }

  String get title {
    // Default implementation is to return the kind of event.
    return this.kind;
  }

  factory Event.fromJson(Map<String, dynamic> json) {
    Map<String, dynamic> data = json['event_data'];
    var materialDate = DateTime.parse(data['MaterialDate']);
    var zid = data['ZID'];
    String kind = json['kind'];
    Map<String, dynamic> event_data = json['event_data'];
    switch (kind) {
      case 'born': {
        return BornEvent(kind, materialDate, zid, event_data);
      } break;
      case 'moved_within':
      case 'moved_in':
      case 'moved_out': {
        return MoveEvent(kind, materialDate, zid, event_data);
      } break;
      case 'registered': {
        return RegisterEvent(kind, materialDate, zid, event_data);
      } break;
      case 'married': {
        return MarriedEvent(kind, materialDate, zid, event_data);
      } break;
      default:
        return Event(
            kind: kind,
            materialDate: materialDate,
            zid: zid,
            data: event_data
        );
    }
  }
}

Map<String, String> _dynamicToMap(Map<String, dynamic> data, {String prefix = ""}) {
  Map<String, String> contents = {};
  data.forEach((_key, value) {
    String key = prefix + _key;
    switch (value.runtimeType.toString()) {
      case 'List<dynamic>': {
        contents[key] = value.reduce((value, element) => value + ', ' + element);
      } break;
      case '_JsonMap': {
        Map<String, String> jsonMap = _dynamicToMap(value, prefix: key + ":");
        jsonMap.forEach((key, value) {contents[key] = value;});
      } break;
      default: {
        contents[key] = value.toString();
      }
    }
  });
  return contents;
}

class BornEvent extends Event {
  BornEvent(kind, materialDate, zid, data)
      : super(kind: kind, materialDate: materialDate, zid: zid, data: data) {}

  String get title {
    return "Geboorte van ${data['Firstname']} ${data['Lastname']}";
  }
}

class MoveEvent extends Event {
  MoveEvent(kind, materialDate, zid, data)
      : super(kind: kind, materialDate: materialDate, zid: zid, data: data) {}

  String get title {
    String eid = data['PersonEID'];
    return "Verhuizing van ${this._getPersonName(eid)}\n" +
           "naar ${this._getAddress() ?? 'andere gemeente'}";
  }
}

class RegisterEvent extends Event {
  RegisterEvent(kind, materialDate, zid, data)
      : super(kind: kind, materialDate: materialDate, zid: zid, data: data) {}

  String get title {
    String eid = data['PersonEID'];
    return "Inschrijving van ${this._getPersonName(eid)}\n" +
           "op adres ${this._getAddress()}";
  }
}

class MarriedEvent extends Event {
  MarriedEvent(kind, materialDate, zid, data)
      : super(kind: kind, materialDate: materialDate, zid: zid, data: data) {}

  String get title {
    var participants = this.data['Participants'].reduce((value, element) =>
    this._getPersonName(value) + ' en ' + this._getPersonName(element));
    return "Huwelijk tussen ${participants}";
  }
}
