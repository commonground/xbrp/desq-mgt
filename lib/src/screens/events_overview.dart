import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:xbrpmgt/src/models/event.dart';
import 'package:xbrpmgt/src/models/event_store.dart';
import 'package:xbrpmgt/src/models/zone.dart';

class EventsOverview extends StatefulWidget {
  final Zone zone;

  EventsOverview({Key key, @required this.zone}) : super(key: key);

  @override
  _EventsOverviewState createState() => _EventsOverviewState();
}

class _EventsOverviewState extends State<EventsOverview> {
  // Show events, every event on a new row
  List<Event> events;

  @override
  void initState() {
    super.initState();
    events = eventStore.events[widget.zone];
  }

  Widget build(BuildContext context) {
    return Container(
        width: 500,
        height: MediaQuery.of(context).size.height * 0.8,
        child: ListView.builder(
            itemCount: events.length,
            itemBuilder: (BuildContext context, int index) {
              Event item = events[index];
              return EventCard(event: item);
            }
        )
    );
  }
}

class EventCard extends StatefulWidget {
  final Event event;

  EventCard({Key key, @required this.event}) : super(key: key);

  @override
  _EventCardState createState() => _EventCardState();
}

class _EventCardState extends State<EventCard> {
  // Default is to hide event contents
  bool hideContent = true;

  @override
  Widget build(BuildContext context) {
    // Always show material date
    String eventText = DateFormat('dd MMMM yyyy – hh:mm')
        .format(widget.event.materialDate);

    // Show contents when asked for
    if (!hideContent) {
      eventText += "\n";
      widget.event.contents.forEach((key, value) {
        eventText += "\n${key} = ${value}";
      });
    }

    return Card(
        child: ListTile(
          leading: Icon(Icons.date_range),
          title: Text(widget.event.title),
          subtitle: Text(eventText),
          trailing:  IconButton(
            icon: Icon(hideContent ? Icons.keyboard_arrow_down
                                   : Icons.keyboard_arrow_up),
            tooltip: 'Show event contents',
            onPressed: () {
              setState(() {
                hideContent = !hideContent;
              });
            },
          ),
          // onTap: () {},
        )
    );
  }
}
