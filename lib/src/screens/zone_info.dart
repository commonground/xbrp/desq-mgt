import 'package:flutter/material.dart';
import 'package:xbrpmgt/src/models/event_store.dart';
import 'package:xbrpmgt/src/models/zone.dart';
import 'package:xbrpmgt/src/screens/events_overview.dart';

class ZoneInfoScreen extends StatefulWidget {
  @override
  _ZoneInfoScreenState createState() => _ZoneInfoScreenState();
}

class _ZoneInfoScreenState extends State<ZoneInfoScreen> {
  Future<bool> initialised;

  @override
  void initState() {
    super.initState();
    this.initialised = eventStore.init();
  }

  String zoneId = null;

  Widget build(BuildContext context) {
    return FutureBuilder<bool>(
      future: initialised,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          List<Zone> _zones = eventStore.zones;
          return Scaffold(
              appBar: AppBar(
                title: const Text('Overzicht')
              ),
              body: Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                    Text('Gemeente',
                        style: Theme.of(context).textTheme.headline4),
                    Padding(
                      padding: EdgeInsets.all(10.0),
                      // Dropdown with all zones to choose from
                      child: DropdownButton<String>(
                        value: zoneId,
                        icon: Icon(Icons.arrow_downward),
                        iconSize: 24,
                        elevation: 16,
                        style: TextStyle(color: Colors.deepPurple),
                        underline: Container(
                          height: 2,
                          color: Colors.deepPurpleAccent,
                        ),
                        onChanged: (String newValue) {
                          setState(() {
                            zoneId = newValue;
                          });
                        },
                        items: _zones
                            .map<DropdownMenuItem<String>>((Zone zone) {
                              return DropdownMenuItem<String>(
                                value: zone.id,
                                child: Text(zone.gemeente),
                              );
                            }).toList(),
                      ),
                    ),
                    _BuildZoneImage(_zones, zoneId),
                  ]
                  ),
                  // Show events for the chosen zone
                  if (zoneId != null)
                    EventsOverview(zone: _zones.firstWhere((zone) => zone.id == zoneId),
                        key: Key(zoneId)),
                ],
              ));
        } else if (snapshot.hasError) {
          return Text("${snapshot.error}");
        }
        // By default, show a loading spinner.
        return Scaffold(
            body: Center(
              child: SizedBox(
                width: 40,
                height: 40,
                child: CircularProgressIndicator(),
              ),
            ));
      },
    );
  }
}

Widget _BuildZoneImage(List<Zone> zones, String zoneId) {
  // Show the image or logo of the zone in the given list of zones
  // with the given zoneId
  const double width = 150;
  const double height = 100;

  Zone zone = zones.firstWhere((element) => element.id == zoneId,
    orElse: () => null);
  if (zone != null) {
    // Zone is found, show image
    return Image.asset(
        'assets/images/${zone.gemeente.toLowerCase()}.png',
        width: width,
        height: height
    );
  } else {
    // Zone is not found, show an empty container
    return Container(
        width: width,
        height: height
    );
  }
}
