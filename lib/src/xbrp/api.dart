import 'dart:convert';

import 'package:xbrpmgt/src/models/event.dart';
import 'package:xbrpmgt/src/models/zone.dart';
import 'package:http/http.dart' as http;

Future<List<Zone>> fetchZones() async {
  final String url = 'http://localhost:2000/api/list-eventservices';
  final response = await http.get(url);

  if (response.statusCode == 200) {
    // If the server did return a 200 OK response,
    // then parse the JSON.
    final Map<String, dynamic> data = jsonDecode(response.body);
    var eventservices = data['eventservices'];
    List<Zone> zones = eventservices.entries.map<Zone>(
            (e) => Zone.fromJson(e.value)).toList();
    return zones;
  } else {
    // If the server did not return a 200 OK response,
    // then throw an exception.
    throw Exception('Failed to load zones');
  }
}

Future<List<Event>> fetchEvents(Zone zone) async {
  final String url = "http://localhost:${zone.port}/api/get-events/all";
  final response = await http.get(url);

  if (response.statusCode == 200) {
    // If the server did return a 200 OK response,
    // then parse the JSON.
    final Map<String, dynamic> data = jsonDecode(response.body);
    var events = data['events'];
    List<Event> eventList = events.map<Event>((e) => Event.fromJson(e)).toList();
    // Sort, newest events on top
    eventList.sort((e1, e2) => (e1.materialDate.isBefore(e2.materialDate) ? 1 : 0));
    return eventList;
  } else {
    // If the server did not return a 200 OK response,
    // then throw an exception.
    throw Exception('Failed to load events');
  }
}
