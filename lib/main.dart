import 'package:flutter/material.dart';
import 'package:xbrpmgt/src/screens/login.dart';
import 'package:xbrpmgt/src/screens/zone_info.dart';

void main() {
  runApp(ManagementApp());
}

final String ZoneInfoRoute = '/zone_info';

class ManagementApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      routes: {
        // '/': (context) => LoginScreen(),
        '/': (context) => ZoneInfoScreen(),
        ZoneInfoRoute: (context) => ZoneInfoScreen(),
      },
    );
  }
}
