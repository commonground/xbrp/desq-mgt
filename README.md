# xbrpmgt

Simple Flutter project voor een management interface voor het XBRP prototype.

De management interface is voorzien van een demo toegangsbeveiliging.
Elke gebruikersnaam-wachtwoord combinatie verleent in deze eerste versie toegang tot de applicatie.

De applicatie bestaat in haar eerste versie uit een register overzicht.
Er kan een keuze worden gemaakt uit alle registers die in het prototype een tol spelen.
Wanneer een register wordt gekozen worden de opgeslagen events uit het betreffende register getoond.

De data is mock data die in een volgende versie wordt vervangen door data uit het XBRP backend.

## Getting Started

De applicatie kan worden gestart door middel van:

`flutter run -d chrome`

## Indeling

De XBRP data models staan in lib/src/models.

De XBRP screens staat in lib/src/screens.
NB: Het events overzicht is feitelijk een widget maar staat toch in deze folder.
In een volgende versie moet dit een andere plaats krijgen.
Dat geldt ook voor de register kiezer die nu onderdeel is van het register info screen.

Enkele utility widgets staan in lib/src/widgets
In de eerste versie staat hier een animated progess indicator widget die is gekopieerd van de flutter website.

De communicatie met de XBRP API staat in lib/src/api
 